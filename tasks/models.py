from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.

class Task(models.Model):
    class Status(models.TextChoices):
        TODO = "TODO", _("Todo")
        INPROGRESS = "INPROGRESS", _("In Progress")
        COMPLETED = "COMPLETED", _("Completed")
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=150)
    status = models.CharField(max_length=50, choices=Status.choices, default=Status.TODO)
    assigned_to = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, blank=False, on_delete=models.CASCADE)


class User(models.Model):
    user_name = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    is_admin = models.BooleanField(default=False)
