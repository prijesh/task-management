import json
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from tasks.models import Task

def home(request):
    user = request.user
    if user.is_staff:
        return HttpResponseRedirect("/admin/tasks/task/")
    tasks = Task.objects.filter(assigned_to=user.pk).values()
    return render(request=request, context={"tasks":tasks, "task_list": list(tasks)}, template_name="tasks/task.html")

def user_login(request):
    userName = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(request, username=userName, password=password)
    if user is None:
        return render(request=request, context={"message": "invalid user"}, template_name="registration/login.html")
    login(request, user)
    return  HttpResponseRedirect("/tasks/home/")

@csrf_exempt
def updateStatus(request):
    payload = json.loads(request.body)
    task = Task.objects.get(id=payload["id"])
    task.status = payload["status"]
    task.save(update_fields=["status"])
    return True



