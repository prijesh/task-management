from django.urls import path
from . import views
from django.contrib.auth.views import LoginView,LogoutView


urlpatterns = [
    path('login/', LoginView.as_view(), name="login"),
    path('login-user/',views.user_login),
    path('updateStatus/', views.updateStatus),
    path('home/', views.home),
]